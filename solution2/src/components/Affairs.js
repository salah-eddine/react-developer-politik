import React, { Component } from 'react'
import _ from 'lodash'
import { fetchAffairs } from 'api'
import { stableSort, getSorting } from 'utils'
import Table from 'components/Table'

class Affairs extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      order: 'asc',
      orderBy: 'updated',
      updated: null
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    const response = await fetchAffairs()
    if (response && response.status === 200) {
      this.setState({ data: response.data })
    }
  }

  handleRequestSort = property => {
    const orderBy = property
    let order = 'desc'

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc'
    }

    this.setState({ order, orderBy })
  }

  render() {
    const { data, order, orderBy, updated } = this.state
    if (_.isEmpty(data)) {
      return <div>Loading...</div>
    }
    const columns = _.keys(_.first(data))
    return (
      <div>
        <h1>Affairs</h1>
        <div>
          <h2>Sort actions</h2>
          <button onClick={() => this.handleRequestSort('updated')}>
            Sort by date
          </button>
        </div>
        <div>
          <h2>Filters</h2>
          <input
            value={updated}
            onChange={e => this.setState({ updated: e.target.value })}
            placeholder="Enter a date"
          />
        </div>
        <div>
          <h2>Values</h2>
          <Table
            columns={columns}
            rows={stableSort(data, getSorting(order, orderBy)).filter(
              a => _.isEmpty(updated) || a.updated.toString().includes(updated)
            )}
          />
        </div>
      </div>
    )
  }
}

export default Affairs
