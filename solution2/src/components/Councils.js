import React, { Component } from 'react'
import _ from 'lodash'
import { fetchCouncils } from 'api'
import { stableSort, getSorting } from 'utils'
import Table from 'components/Table'

class Councils extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      order: 'asc',
      orderBy: 'id',
      id: ''
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    const response = await fetchCouncils()
    if (response && response.status === 200) {
      this.setState({ data: response.data })
    }
  }

  handleRequestSort = property => {
    const orderBy = property
    let order = 'desc'

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc'
    }

    this.setState({ order, orderBy })
  }

  render() {
    const { data, order, orderBy, name } = this.state
    if (_.isEmpty(data)) {
      return <div>Loading...</div>
    }
    const columns = _.keys(_.first(data))
    return (
      <div>
        <h1>Councils</h1>
        <div>
          <h2>Sort actions</h2>
          <button onClick={() => this.handleRequestSort('name')}>
            Sort by name
          </button>
        </div>
        <div>
          <h2>Filters</h2>
          <input
            value={name}
            onChange={e => this.setState({ name: e.target.value })}
            placeholder="Enter an name"
          />
        </div>
        <div>
          <h2>Values</h2>
          <Table
            columns={columns}
            rows={stableSort(data, getSorting(order, orderBy)).filter(
              a =>
                _.isEmpty(name) ||
                a.name
                  .toString()
                  .toUpperCase()
                  .includes(name.toUpperCase())
            )}
          />
        </div>
      </div>
    )
  }
}

export default Councils
