import React, { Component } from 'react'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import Councillors from 'components/Councillors'
import Affairs from 'components/Affairs'
import Councils from 'components/Councils'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/councillors">Councillors</Link>
            </li>
            <li>
              <Link to="/affairs">Affairs</Link>
            </li>
            <li>
              <Link to="/councils">Councils</Link>
            </li>
          </ul>
          <Route exact path="/councillors" component={Councillors} />
          <Route path="/affairs" component={Affairs} />
          <Route path="/councils" component={Councils} />
        </div>
      </Router>
    )
  }
}

export default App
