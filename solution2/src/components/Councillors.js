import React, { Component } from 'react'
import _ from 'lodash'
import { fetchCouncillors } from 'api'
import { stableSort, getSorting } from 'utils'
import Table from 'components/Table'

class Councillors extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      order: 'asc',
      orderBy: 'id',
      id: '',
      firstName: '',
      lastName: ''
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    const response = await fetchCouncillors()
    if (response && response.status === 200) {
      this.setState({ data: response.data })
    }
  }

  handleRequestSort = property => {
    const orderBy = property
    let order = 'desc'

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc'
    }

    this.setState({ order, orderBy })
  }

  render() {
    const { data, order, orderBy, id, firstName, lastName } = this.state
    if (_.isEmpty(data)) {
      return <div>Loading...</div>
    }
    const columns = _.keys(_.first(data))
    return (
      <div>
        <h1>Councillors</h1>
        <div>
          <h2>Sort actions</h2>
          <button onClick={() => this.handleRequestSort('id')}>
            Sort by id
          </button>
          <button onClick={() => this.handleRequestSort('firstName')}>
            Sort by firstname
          </button>
          <button onClick={() => this.handleRequestSort('lastName')}>
            Sort by lastname
          </button>
        </div>
        <div>
          <h2>Filters</h2>
          <input
            value={id}
            onChange={e => this.setState({ id: e.target.value })}
            placeholder="Enter an id"
          />
          <input
            value={firstName}
            onChange={e => this.setState({ firstName: e.target.value })}
            placeholder="Enter a firstName"
          />
          <input
            value={lastName}
            onChange={e => this.setState({ lastName: e.target.value })}
            placeholder="Enter a lastName"
          />
        </div>
        <div>
          <h2>Values</h2>
          <Table
            columns={columns}
            rows={stableSort(data, getSorting(order, orderBy))
              .filter(a => _.isEmpty(id) || a.id.toString().includes(id))
              .filter(
                a =>
                  _.isEmpty(firstName) ||
                  a.firstName
                    .toString()
                    .toUpperCase()
                    .includes(firstName.toUpperCase())
              )
              .filter(
                a =>
                  _.isEmpty(lastName) ||
                  a.lastName
                    .toString()
                    .toUpperCase()
                    .includes(lastName.toUpperCase())
              )}
          />
        </div>
      </div>
    )
  }
}

export default Councillors
