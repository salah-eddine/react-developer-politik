import axios from 'axios'

const HttpClient = axios.create({
  // baseURL: 'http://192.168.0.143:8080/',
  baseURL: 'http://ws-old.parlament.ch',
  headers: {
    Accept: 'text/json'
  }
})

export const fetchCouncillors = () => {
  return HttpClient.get('councillors').catch(error => console.log(error))
}

export const fetchAffairs = () => {
  return HttpClient.get('affairs').catch(error => console.log(error))
}

export const fetchCouncils = () => {
  return HttpClient.get('councils').catch(error => console.log(error))
}
